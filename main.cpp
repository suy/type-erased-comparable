#include <iostream>
#include <typeindex>

#include "te.hpp"

using std::cout;
using std::endl;

namespace boost_te {

namespace te = boost::te;

#if 0
struct Introspectable : te::poly<Introspectable> {
    const std::type_info& type() const {
        return te::call<const std::type_info&>([](auto const& self) {
            return typeid(self);
        }, *this);
    }
};
#endif

struct One {
#if 0
    template<typename T>
    bool operator==(const T&) const {
        cout << __PRETTY_FUNCTION__ << endl;
        if constexpr(std::is_same_v<One, T>)
            return true;
        return false;
    }
#else
     bool operator==(const One&) const {
         return true;
     }
#endif
};

struct Two {
#if 0
    template<typename T>
    bool operator==(const T&) const {
        cout << __PRETTY_FUNCTION__ << endl;
        if constexpr(std::is_same_v<Two, T>)
            return true;
        return false;
    }
#else
     bool operator==(const Two&) const {
         return true;
     }
#endif
};


struct Comparable : te::poly<Comparable> {
    using te::poly<Comparable>::poly;

#if 0
    template<typename Other>
    bool operator==(const Other& other) const {
        cout << __PRETTY_FUNCTION__ << " " << static_cast<void*>(other) << endl;
        return false;
    }
#endif

    bool operator==(const Comparable& other) const {
        auto body = [](auto const& self, Comparable const& other) {
            // `self` is the "downcast" type (One/Two), but `other` is
            // Comparable, the interface. We need to figure out how to get the
            // right type.

            // This doesn't really work, as the types are never the same.
            // if constexpr (std::is_same_v<decltype(self), decltype(other)>)
            //     return self == other;
            // return false;

            // This doesn't compile, as One or Two can (and should) only compare
            // with themselves, but `other` is of type Comparable.
//            return self == other;

            // Not ideal, as we know for sure that we are not the same when the
            // type is not the same, but we don't know if two objects of the
            // same type are the same. We need to cast `other` to the type of
            // `self`. Now `other` is Comparable, while `self` is the "real"
            // object put into a Comparable (e.g. One, Two).
            const bool sameType = other.type() == std::type_index(typeid(self));
            // return sameType;

            // Would call e.g. self.One::operator==(const Comparable& other)
            // (but only if One::operator== is a template, otherwise it's a
            // compilation error).
            if (!sameType)
                return false;

            // Attempt to cast `other` to the same type of `self`.
            using SelfType = decltype(self);

            auto innerBody = [] (auto const& innerSelf, const SelfType& innerOther) {
                cout << __PRETTY_FUNCTION__ << endl;
                if constexpr (std::is_same_v<decltype(innerSelf), SelfType>)
                    return false;
                else
                    return innerSelf == innerOther;
            };
            // Crashes.
            return te::call<bool>(innerBody, other, self);
        };

        return te::call<bool>(body, *this, other);
    }

    std::type_index type() const {
        return te::call<std::type_index>([](auto const& self) {
            return std::type_index(typeid(self));
        }, *this);
    }
};

void main()
{
    Comparable one = One{};
    Comparable two = Two{};
    cout << one.type().name() << endl;
    cout << two.type().name() << endl;
    cout << endl << "one==one " << (one == one) << endl;
    cout << endl << "one==two " << (one == two) << endl;
    cout << endl << "two==two " << (two == two) << endl;
}

} // boost_te

////////////////////////////////////////////////////////////////////////////////

namespace entty_poly {

void main() {

}

} // entty_poly

////////////////////////////////////////////////////////////////////////////////

int main()
{
    boost_te::main();
    entty_poly::main();
}
